next ui is an open-source front-end library focused on site speed.

Avoid CDN's and load the files directly on your site.

You may also pick and choose what to use and what not to use. This library is meant to be fully modular.